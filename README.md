CheeseCutter patches
====================
My personal patches to Cheesecutter 2 to the sourcecode @ http://theyamo.kapsi.fi/ccutter

These patches have two purposes:

1. Make CheeseCutter 2 work on my Macbook Pro keyboard.
2. Help creating a better version of CheeseCutter that works on all platforms.
